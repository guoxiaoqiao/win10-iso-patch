

if([System.IO.File]::Exists("C:\win10\install.wim")){
  dism /unmount-wim /mountdir:C:\win10\wim  /discard

  dism /Cleanup-Wim

  rm C:\win10\install.wim
}

copy H:\sources\install.wim c:\win10\

Set-ItemProperty C:\win10\install.wim -name IsReadOnly -value $false

dism /mount-wim /wimfile:C:\win10\install.wim /index:1 /mountdir:C:\win10\wim

dism /image:C:\win10\wim /Add-Package /PackagePath:F:\win10-update

Dism.exe /image:C:\win10\wim /Cleanup-Image /AnalyzeComponentStore

dism /image:C:\win10\wim /cleanup-image /startcomponentcleanup

Dism.exe /image:C:\win10\wim /Cleanup-Image /AnalyzeComponentStore

dism /unmount-wim /mountdir:C:\win10\wim  /commit

dism /Cleanup-Wim

if([System.IO.File]::Exists("C:\win10\install-large.wim")){
  rm C:\win10\install-large.wim
}

mv C:\win10\install.wim C:\win10\install-large.wim

Dism /Export-Image /SourceImageFile:C:\win10\install-large.wim /SourceIndex:1 /DestinationImageFile:c:\win10\install.wim